"""
    Le module de ``chiffrement``
    ============================
    Permet de chiffrer un texte en décalant chaque caractère par 8 slots(par défaut) du tableau UTF-8
    auteurs: MONT Louit et MONTOUCHET Teddy
"""
from tkinter import *
from tkinter.messagebox import *
from string import *
import string


def decode(message, rotate=13, isCirculer=False):
    """
    Decode le message passé en paramètre
    Appelle la fonction encoding originelle
    :rtype: string
    """
    return encoding(message, -rotate, isCircular)

def encoding(message, rotate=13, isCircular=False):
    """
    Code le message passé en caractère en le décalant de 13(par défaut) dans l'alphabet UTF-8(par défaut)
     ,si isCircular est à True, le décale dans l'Alphabet normal)
    :rtype: string
    """
    list_msg = list(message)
    for index in range(len(list_msg)-1):
        if(isCircular):
            list_msg[index] = shiftLetterCircular(list_msg[index], rotate)
        else:
            list_msg[index] = chr(ord(list_msg[index])+rotate)
    message = ''.join(list_msg)
    return message

def shiftLetterCircular(letter, rotate=13):
    """
        Décale chaque caractère par 12(défaut), seulement les lettres
        :rtype: char
    """
    if(string.ascii_letters.find(letter) != -1):
        if(letter.islower()):
            alphabet = string.ascii_lowercase
            firstpart = alphabet[alphabet.find(letter):]
            secondpart = alphabet[:alphabet.find(letter)]
            circular = firstpart+secondpart
            letter = circular[rotate % 26]
        if(letter.isupper()):
            alphabet = string.ascii_uppercase
            firstpart = alphabet[alphabet.find(letter):]
            secondpart = alphabet[:alphabet.find(letter)]
            circular = firstpart+secondpart
            letter = circular[rotate % 26]
    return letter


def callback_encoding(key=0, isCircular=False):
    """
    Callback pour encoder le texte dans le label à partir de l'entrée
    """
    resultattext.delete('1.0', END)
    resultat = encoding(chaine.get("0.0", END), key, isCircular) #ce qui décode
    resultattext.insert(END, resultat)    
    # showinfo("%s"%entry.get(),"%s"%chiffrement(entry.get()))


def callback_decoding(key=0, isCircular=False):
    """
    Callback pour décoder le texte dans le label à partir de l'entrée
    Appelle la fonction callback_encoding originelle
    """
    callback_encoding(key, isCircular)
    
def tryIfInt(intTry):
    try:
        return int(intTry)
    except ValueError:
        return False

"""
    Main
"""
frame = Tk()
frame.title("Chiffrement Cesar")
frame.minsize(400, 300)
frame.columnconfigure(0, weight=1)
frame.columnconfigure(2, weight=1)

chaine = Text(frame, wrap='word', height='10')
chaine.insert(END, '')
chaine.grid(row=1, sticky='nsew', columnspan='3')

keytext = StringVar()
keytext.set("Clé:")
keylabel = Label(frame, textvariable=keytext)
keylabel.grid(row=2, column='1')

keyt = Text(frame, wrap='word', width='5', height='1')
keyt.insert(END, "")
keyt.grid(row=3, column='1')


btn_chiffrement = Button(frame, text="Chiffrement", command=lambda: callback_encoding(
    tryIfInt(keyt.get("0.0", END)), isCircular.get()))
btn_chiffrement.grid(row=4, column='0')

isCircular = BooleanVar()
isCircular.set(False)
cb = Checkbutton(frame, text="Circulaire", variable=isCircular)
cb.bind("<Button-1>", isCircular.get())
cb.grid(row=4, column='1')

btn_dechiffrement = Button(frame, text="Déchiffrement",
                           command=lambda: callback_decoding(tryIfInt(keyt.get("0.0", END)), isCircular.get()))
btn_dechiffrement.grid(row=4, column='2')

resultattext = Text(frame, wrap="word", height='10')
resultattext.insert(END, "")
resultattext.grid(row=5, sticky='nsew', columnspan='3')

key = 13

frame.mainloop()
