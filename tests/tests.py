import unittest

from chiffrementCesar import encoding
from chiffrementCesar import shiftLetterCircular

class TestCesarChiffrement(unittest.TestCase):    
    def test_chiffrement_dechiffrement(self):
        key=13
        #chiffrement
        self.assertEqual(encoding("lab",key),"yno")
        self.assertEqual(encoding(""),"")
        #dechiffrement
        self.assertEqual(encoding("XQN",-key),"KDA")
        self.assertEqual(encoding(""),"")

    def test_shiftLetterCircular(self):
        key=13
        self.assertEqual(shiftLetterCircular('é',key),'é')
        self.assertEqual(shiftLetterCircular('e',key),'r')

    def test_chiffrementcirculaire(self):
        key=13
        self.assertEqual(encoding("yolo",key,True),"lbyb")
        self.assertEqual(encoding("",key,True),"")